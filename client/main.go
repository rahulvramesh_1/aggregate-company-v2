package main

import (
	"fmt"
	"strconv"
	"time"

	pb "bitbucket.org/evhivetech/aggregate-company-v2/client/entities/company"
	ptypes "github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn               *grpc.ClientConn
		responseTimeCreate int64
		responseTimeUpdate int64
	)

	totalLoop := int64(30)
	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceCompanyClient(conn)

	ts := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("\nsent at : %d", ts)

	for i := int64(0); i < totalLoop; i++ {

		timestamp, test := ptypes.TimestampProto(time.Now())

		if test != nil {
			fmt.Print(test)
		}

		response, err := c.Add(context.Background(), &pb.RequestProcessCompany{
			Process: &pb.Process{
				Id:            "066f2766-8bf5-11e8-9eb6-012345fb1459",
				Name:          "Process A Create",
				Origin:        "Coordinator A",
				Timestamp:     timestamp,
				TotalSequence: 5,
				UserId:        1,
				SubProcess: &pb.SubProcess{
					Id:         "01234578-8bf5-11e8-9eb6-529269fb1459",
					Name:       pb.SubProcessName_CREATE_COMPANY,
					Timestamp:  timestamp,
					SequenceOf: 1,
				},
			},
			Company: &pb.Company{
				Id:       1,
				Name:     "New company",
				Email:    "info@cocowork.co",
				Bio:      "Bio",
				Industry: "Coworking",
				Version:  1,
			},
		})

		if err != nil {
			log.Fatalf("Error when calling add : %s", err)
		}

		receivedAt := time.Now().UnixNano() / int64(time.Millisecond)
		responseTime := (receivedAt - ts)
		responseTimeCreate += responseTime
		fmt.Printf("\nreceived at : %d, response time : %d", receivedAt, responseTime)
		fmt.Printf("\n Response from server: \n Process=%s \n company=%s \n\n\n", response.GetProcess(), response.GetCompany())

		ts = time.Now().UnixNano() / int64(time.Millisecond)
		fmt.Printf("\nsent at : %d", ts)
		timestamp, _ = ptypes.TimestampProto(time.Now())
		response, err = c.Update(context.Background(), &pb.RequestProcessCompany{
			Process: &pb.Process{
				Id:            "066f2766-8bf5-11e8-9eb6-529269fb1459",
				Name:          "Process A Update",
				Origin:        "Coordinator A",
				Timestamp:     timestamp,
				TotalSequence: 5,
				UserId:        1,
				SubProcess: &pb.SubProcess{
					Id:         "066f2e78-8bf5-11e8-9eb6-529269fb1459",
					Name:       pb.SubProcessName_UPDATE_COMPANY,
					Timestamp:  timestamp,
					SequenceOf: 1,
				},
			},
			Company: &pb.Company{
				Id:       response.GetCompany().Id,
				Name:     "Cocowork company" + strconv.Itoa(int(i)),
				Email:    response.GetCompany().Email + strconv.Itoa(int(i)),
				Bio:      response.GetCompany().Bio + strconv.Itoa(int(i)),
				Industry: response.GetCompany().Industry + strconv.Itoa(int(i)),
				Version:  response.GetCompany().Version,
			},
		})
		if err != nil {
			log.Fatalf("Error when calling update : %s", err)
		}

		receivedAt = time.Now().UnixNano() / int64(time.Millisecond)
		responseTime = (receivedAt - ts)
		responseTimeUpdate += responseTime
		fmt.Printf("\nreceived at : %d response time : %d", receivedAt, responseTime)
		fmt.Printf("\n Response from server: \n Process=%s \n company=%s \n", response.GetProcess(), response.GetCompany())
	}

	fmt.Printf("average response time create : %d", responseTimeCreate/totalLoop)
	fmt.Printf("average tme update : %d", responseTimeUpdate/totalLoop)
}
