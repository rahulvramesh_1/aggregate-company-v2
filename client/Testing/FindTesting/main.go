package main

import (
	"fmt"
	"time"

	pb "bitbucket.org/evhivetech/aggregate-company-v2/client/entities/company"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceCompanyClient(conn)

	timestamp, _ := ptypes.TimestampProto(time.Now())

	response, err := c.Add(context.Background(), &pb.RequestProcessCompany{
		Process: &pb.Process{
			Id:            "066f2766-8bf5-11e8-9eb6-529123456459",
			Name:          "Process A Create",
			Origin:        "Coordinator A",
			Timestamp:     timestamp,
			TotalSequence: 5,
			UserId:        1,
			SubProcess: &pb.SubProcess{
				Id:         "066f2766-8bf5-11e8-9eb6-529123456490",
				Name:       pb.SubProcessName_CREATE_COMPANY,
				Timestamp:  timestamp,
				SequenceOf: 1,
			},
		},
		Company: &pb.Company{
			Id:       1,
			Name:     "cocowork",
			Email:    "info@cocowork.co",
			Bio:      "Bio",
			Industry: "Coworking",
			Version:  1,
		},
	})

	if err != nil {
		log.Fatalf("Error when calling add : %s", err)
	} else {
		log.Infof("Add data success")
		fmt.Printf("\n %s", response.GetProcess())
		fmt.Printf("\n %s", response.GetCompany())
	}

	requestFindOneCompany := pb.RequestFindOneCompany{
		Company: &pb.SearchCompany{},
		Key:     "name",
	}

	requestFindOneCompany.Company.Name = append(requestFindOneCompany.Company.Name, "Cocowork")

	request, err := c.FindOne(context.Background(), &requestFindOneCompany)

	if err != nil {
		log.Fatalf("Error when finding company : %s", err)
	} else {
		log.Infof("Get data from key name")
		fmt.Printf("\n %s", request.GetCompany())
	}
}
