package main

import (
	"fmt"
	"io"

	pb "bitbucket.org/evhivetech/aggregate-company-v2/client/entities/company"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceCompanyClient(conn)

	requestFindAllCompany := pb.RequestFindAllCompany{
		Company: &pb.SearchCompany{},
		Pagination: &pb.RequestPagination{
			Limit:  100,
			Offset: 0,
		},
		Key: "name",
	}

	requestFindAllCompany.Company.Name = append(requestFindAllCompany.Company.Name, "Company")
	stream, err := c.FindAll(context.Background(), &requestFindAllCompany)

	if err != nil {
		log.Fatalf("Error when finding company : %s", err)
	} else {
		var responsePagination pb.ResponsePagination
		i := 0
		fmt.Println("Data from stream: ")
		for {
			dataFromServer, err := stream.Recv()
			if i == 0 {
				responsePagination.TotalPage = dataFromServer.Pagination.TotalPage
				responsePagination.TotalItems = dataFromServer.Pagination.TotalItems
				responsePagination.Limit = dataFromServer.Pagination.Limit
				responsePagination.PageNumber = dataFromServer.Pagination.PageNumber
			}

			if err == io.EOF {
				break
			}

			if err != nil {
				log.Println("Error", err)
				break
			}

			fmt.Printf("Id: %d", dataFromServer.Company[i].Id)
			i += 1
		}

		fmt.Println("\nPagination")
		fmt.Printf("Total Page: %d, Total Items: %d, Limit: %d, Page Number: %d",
			responsePagination.TotalPage,
			responsePagination.TotalItems,
			responsePagination.Limit,
			responsePagination.PageNumber)
	}
}
