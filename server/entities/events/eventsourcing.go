package es

import "time"

const ENTITY_NAME = "COMPANY"
const COMPANY_ADDED = "STATUS_ADDED"
const NAME_CHANGED = "NAME_CHANGED"
const EMAIL_CHANGED = "EMAIL_CHANGED"
const BIO_CHANGED = "BIO_CHANGED"
const INDUSTRY_CHANGED = "INDUSTRY_CHANGED"

type EventSource struct {
	ID        int64
	Name      string
	Version   int64
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Event struct {
	ID               int64
	ProcessID        string
	ProcessCreatedAt time.Time
	SubProcessID     string
	EventSourceID    int64
	EventSourceName  string
	Version          int64
	Sequence         int32
	Type             string
	Data             string
	CreatedAt        time.Time
}

type Snapshot struct {
	EventSourceName string
	EventSourceID   int64
	Version         int64
	Data            string
	CreatedAt       time.Time
}
