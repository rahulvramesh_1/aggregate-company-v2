package es

import (
	"time"

	jsoniter "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-company-v2/server/entities/company"
	"github.com/golang/protobuf/ptypes"
)

func CopyEvent(e Event) Event {
	return Event{
		ID:               e.ID,
		ProcessID:        e.ProcessID,
		ProcessCreatedAt: e.ProcessCreatedAt,
		SubProcessID:     e.SubProcessID,
		EventSourceID:    e.EventSourceID,
		EventSourceName:  e.EventSourceName,
		Version:          e.Version,
		Sequence:         e.Sequence,
		Type:             e.Type,
		Data:             string(e.Data),
		CreatedAt:        e.CreatedAt,
	}
}

func BuildEventsFromCompany(process *pb.Process, prev *pb.Company, curr *pb.Company) ([]Event, error) {
	var events []Event
	var data []byte

	procTS, err := ptypes.Timestamp(process.GetTimestamp())
	if err != nil {
		return events, err
	}

	event := Event{
		ID:               0,
		ProcessID:        process.GetId(),
		ProcessCreatedAt: procTS,
		SubProcessID:     process.GetSubProcess().GetId(),
		EventSourceID:    prev.GetId(),
		EventSourceName:  ENTITY_NAME,
		Version:          prev.GetVersion(),
		Sequence:         0,
		Type:             "",
		Data:             string(data),
		CreatedAt:        time.Now(),
	}

	if prev.Version > 0 && prev.Id > 0 && process.SubProcess.Name == pb.SubProcessName_UPDATE_COMPANY {
		if prev.Name != curr.Name {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = NAME_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.Email != curr.Email {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = EMAIL_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.Bio != curr.Bio {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = BIO_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.Industry != curr.Industry {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = INDUSTRY_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

	} else {
		data, _ = jsoniter.Marshal(&curr)
		event.Type = COMPANY_ADDED
		event.Data = string(data)
		event.Sequence++
		events = append(events, CopyEvent(event))
	}
	return events, err
}

func BuildCompanyFromEvents(es EventSource, events []Event, ss *pb.Company) (*pb.Company, error) {
	var (
		err     error
		company *pb.Company
		temp    *pb.Company
	)

	//if snapshot exists
	if ss.Id > 0 {
		company = ss
	}

	for _, event := range events {
		jsoniter.Unmarshal([]byte(event.Data), &temp)
		switch eventType := event.Type; eventType {
		case COMPANY_ADDED:
			company = temp
		case NAME_CHANGED:
			company.Name = temp.Name
		case EMAIL_CHANGED:
			company.Email = temp.Email
		case BIO_CHANGED:
			company.Bio = temp.Bio
		case INDUSTRY_CHANGED:
			company.Industry = temp.Industry
		}
	}

	//compensate payload
	company.Id = es.ID
	company.Version = es.Version
	company.CreatedAt, _ = ptypes.TimestampProto(es.CreatedAt)
	company.UpdatedAt, _ = ptypes.TimestampProto(es.UpdatedAt)

	return company, err
}
