package repository

import (
	pb "bitbucket.org/evhivetech/aggregate-company-v2/server/entities/company"
	es "bitbucket.org/evhivetech/aggregate-company-v2/server/entities/events"
)

//Using Command Query segragation
type Q interface {
	FindOne(company *pb.Company, key string) (*pb.Company, error)
	FindAll(company *pb.SearchCompany, limit int, offset int, key string) ([]*pb.Company, error)
	AddOne(company *pb.Company) (*pb.Company, error)
	UpdateOne(company *pb.Company) (*pb.Company, error)
}

type C interface {
	WriteNewStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	AppendToStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	ReadStream(eventSource es.EventSource, minVersion int, maxVersion int) ([]*es.Event, error)

	WriteSnapshot(snapshot *es.Snapshot) (*pb.Company, error)
	ReadSnapshot(snapshot *es.Snapshot) (*pb.Company, error)
	ReadLatestSnapshot(eventSource es.EventSource) (*pb.Company, error)
}
