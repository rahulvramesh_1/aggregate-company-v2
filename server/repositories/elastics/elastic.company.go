package elastics

import (
	"context"
	"errors"
	"strconv"
	"strings"

	json "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-company-v2/server/entities/company"
	"bitbucket.org/evhivetech/aggregate-company-v2/server/repositories"
	"github.com/labstack/gommon/log"
	"github.com/olivere/elastic"
)

type Q_DB struct {
	db *elastic.Client
}

func GetKeyValue(company *pb.Company, key string) (string, error) {
	var keyValue string
	var err error

	switch key {
	case "id":
		keyValue = strconv.Itoa(int(company.Id))
	case "name":
		keyValue = strings.ToLower(company.Name)
	case "email":
		keyValue = strings.ToLower(company.Email)
	case "bio":
		keyValue = strings.ToLower(company.Bio)
	case "industry":
		keyValue = strings.ToLower(company.Industry)
	default:
		err = errors.New("Wrong key")
	}

	return keyValue, err
}

func GetQueryClientDB(db *elastic.Client) repository.Q {
	return &Q_DB{db: db}
}

func (q *Q_DB) AddOne(company *pb.Company) (*pb.Company, error) {
	var err error

	_, err = q.db.Index().
		Index("company").
		Type("doc").
		Id(strconv.Itoa(int(company.Id))).
		BodyJson(company).Refresh("wait_for").Do(context.Background())

	if err != nil {
		log.Error(err)
		return company, err
	}

	return company, err
}

func (q *Q_DB) FindOne(company *pb.Company, key string) (*pb.Company, error) {
	var (
		_company *pb.Company
		keyValue string
		_err     error
	)

	keyValue, _err = GetKeyValue(company, key)
	if _err != nil {
		log.Error(_err)
		return _company, _err
	}

	termQuery := elastic.NewTermQuery(key, keyValue)
	result, err := q.db.Search().
		Index("company").
		Query(termQuery).
		From(0).Size(1).
		Do(context.Background())

	if err != nil {
		log.Error(err)
		return _company, err
	}

	if result.Hits.TotalHits > 0 {
		for _, hit := range result.Hits.Hits {
			err := json.Unmarshal(*hit.Source, &_company)
			if err != nil {
				log.Error(err)
				return _company, err
			}
		}
	}

	return _company, err
}

func (q *Q_DB) FindAll(company *pb.SearchCompany, limit int, offset int, key string) ([]*pb.Company, error) {
	var (
		keyValues []string
		err       error
	)

	listCompany := []*pb.Company{}

	a := 0
	for len(listCompany) < limit {
		_company := &pb.Company{}

		switch key {
		case "id":
			if len(company.Id) != 0 && a < len(company.Id) {
				_company.Id = int64(company.Id[a])
			} else {
				return listCompany, err
			}
		case "name":
			if len(company.Name) != 0 && a < len(company.Name) {
				_company.Name = company.Name[a]
			} else {
				return listCompany, err
			}
		case "email":
			if len(company.Email) != 0 && a < len(company.Email) {
				_company.Email = company.Email[a]
			} else {
				return listCompany, err
			}
		case "bio":
			if len(company.Bio) != 0 && a < len(company.Bio) {
				_company.Bio = company.Bio[a]
			} else {
				return listCompany, err
			}
		case "industry":
			if len(company.Industry) != 0 && a < len(company.Industry) {
				_company.Industry = company.Industry[a]
			} else {
				return listCompany, err
			}
		default:
			err := errors.New("Wrong key")
			return listCompany, err
		}

		keyValue, _err := GetKeyValue(_company, key)
		if _err != nil {
			log.Error(_err)
			return listCompany, _err
		}

		keyValues = append(keyValues, keyValue)

		termQuery := elastic.NewTermQuery(key, keyValues[a])
		result, err := q.db.Search().
			Index("company").
			Query(termQuery).
			From(offset).Size(limit).
			Do(context.Background())

		if err != nil {
			log.Error(err)
			return listCompany, err
		}

		if result.Hits.TotalHits > 0 {
			i := 0
			for _, hit := range result.Hits.Hits {
				tempCompany := &pb.Company{}
				listCompany = append(listCompany, tempCompany)
				err := json.Unmarshal(*hit.Source, &listCompany[i])
				if err != nil {
					log.Error(err)
					return listCompany, err
				}
				i += 1
			}
		}
		a += 1
	}

	return listCompany, err
}

func (q *Q_DB) UpdateOne(company *pb.Company) (*pb.Company, error) {
	var err error
	_, err = q.db.Index().
		Index("company").
		Type("doc").
		Id(strconv.Itoa(int(company.Id))).
		BodyJson(company).
		Do(context.Background())
	if err != nil {
		log.Error(err)
		return company, err
	}
	return company, err
}
