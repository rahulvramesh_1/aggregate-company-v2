package elastics

import (
	"context"

	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
)

func Init(e *elastic.Client) {
	ctx := context.Background()
	// Use the IndexExists service to check if a specified index exists.
	exists, err := e.IndexExists("company").Do(ctx)
	if err != nil {
		// Handle error
		log.Error(err)
	}
	if !exists {
		// Create a new index.
		createIndex, err := e.CreateIndex("company").Do(ctx)
		if err != nil {
			// Handle error
			log.Error(err)
		}
		if !createIndex.Acknowledged {
			log.Error(err)
		}
	}
}
