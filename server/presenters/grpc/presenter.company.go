package grpc_presenter

import (
	"encoding/json"
	"errors"
	"math"

	pb "bitbucket.org/evhivetech/aggregate-company-v2/server/entities/company"
	"bitbucket.org/evhivetech/aggregate-company-v2/server/usecases"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type CompanyGRPCPresenter struct {
	usecase usecases.UsecaseCompany
}

func NewCompanyGRPCPresenter(sv *grpc.Server, uc usecases.UsecaseCompany) {
	p := &CompanyGRPCPresenter{
		usecase: uc,
	}

	pb.RegisterServiceCompanyServer(sv, p)
}

func (p *CompanyGRPCPresenter) FindAll(ctx *pb.RequestFindAllCompany, stream pb.ServiceCompany_FindAllServer) error {
	var (
		err      error
		response *pb.ResponseFindAllCompany
	)

	response = &pb.ResponseFindAllCompany{
		Pagination: &pb.ResponsePagination{
			Limit: ctx.Pagination.Limit,
		},
	}

	if ctx.Pagination.PageNumber == 0 {
		response.Pagination.PageNumber = 1
	} else {
		response.Pagination.PageNumber = ctx.Pagination.PageNumber
	}

	response.Company, err = p.usecase.FindAll(ctx.GetCompany(), int(ctx.Pagination.Limit), int(ctx.Pagination.Offset), ctx.Key)
	if err != nil {
		log.Error(err)
		return err
	}

	if len(response.Company) < 10 {
		response.Pagination.TotalPage = 1
		response.Pagination.TotalItems = int32(len(response.Company))
	} else {
		responseCompanyLengthAsFloat64 := float64(len(response.Company))
		responseCompanyLengthAsint32 := int32(len(response.Company))

		//10 is number of company in one page
		response.Pagination.TotalPage = responseCompanyLengthAsint32 / 10
		if math.Mod(responseCompanyLengthAsFloat64, 10) != 0 {
			response.Pagination.TotalPage += 1
		}

		response.Pagination.TotalItems = responseCompanyLengthAsint32
	}

	for i := 0; i < len(response.Company); i++ {
		if err := stream.Send(response); err != nil {
			return err
		}
	}

	return err
}

func (p *CompanyGRPCPresenter) FindOne(ctx context.Context, params *pb.RequestFindOneCompany) (*pb.ResponseFindOneCompany, error) {
	var (
		err      error
		response *pb.ResponseFindOneCompany
	)

	response = &pb.ResponseFindOneCompany{
		Company: &pb.Company{},
	}

	response.Company, err = p.usecase.FindOne(params.GetCompany(), params.Key)
	if err != nil {
		log.Error(err)
		return response, err
	}
	return response, err
}

func (p *CompanyGRPCPresenter) Add(ctx context.Context, params *pb.RequestProcessCompany) (*pb.ResponseProcessCompany, error) {
	var (
		err      error
		response *pb.ResponseProcessCompany
	)

	response = &pb.ResponseProcessCompany{
		Process: params.GetProcess(),
		Company: params.GetCompany(),
	}

	response.Process, response.Company, err = p.usecase.AddOne(params.GetProcess(), params.GetCompany())
	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *CompanyGRPCPresenter) Update(ctx context.Context, params *pb.RequestProcessCompany) (*pb.ResponseProcessCompany, error) {
	var (
		err      error
		response *pb.ResponseProcessCompany
	)

	response = &pb.ResponseProcessCompany{
		Process: params.GetProcess(),
		Company: params.GetCompany(),
	}

	response.Process, response.Company, err = p.usecase.UpdateOne(params.GetProcess(), params.GetCompany())

	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *CompanyGRPCPresenter) Remove(ctx context.Context, params *pb.RequestProcessCompany) (*pb.ResponseProcessCompany, error) {
	var (
		err      error
		response *pb.ResponseProcessCompany
		proc     *pb.Process
		company  *pb.Company
	)

	proc, company, err = p.usecase.RemoveOne(params.GetProcess(), params.GetCompany())

	response = &pb.ResponseProcessCompany{
		Process: proc,
		Company: company,
	}

	return response, err
}

func (p *CompanyGRPCPresenter) Rollback(ctx context.Context, params *pb.RequestRollbackCompany) (*pb.ResponseRollbackCompany, error) {
	var (
		err      error
		response *pb.ResponseRollbackCompany
	)
	return response, err
}
