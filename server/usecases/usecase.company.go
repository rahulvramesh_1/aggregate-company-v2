package usecases

import (
	"encoding/json"
	"errors"
	"math"
	"time"

	pb "bitbucket.org/evhivetech/aggregate-company-v2/server/entities/company"
	es "bitbucket.org/evhivetech/aggregate-company-v2/server/entities/events"
	"bitbucket.org/evhivetech/aggregate-company-v2/server/repositories"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
)

type UsecaseCompany interface {
	FindOne(params *pb.SearchCompany, key string) (*pb.Company, error)
	FindAll(params *pb.SearchCompany, limit int, offset int, key string) ([]*pb.Company, error)
	Count(params *pb.SearchCompany) (int, error)

	AddOne(proc *pb.Process, company *pb.Company) (*pb.Process, *pb.Company, error)
	UpdateOne(proc *pb.Process, company *pb.Company) (*pb.Process, *pb.Company, error)
	RemoveOne(proc *pb.Process, company *pb.Company) (*pb.Process, *pb.Company, error)
}

type RepositoryCompany struct {
	C repository.C
	Q repository.Q
}

func NewCompanyUseCase(c repository.C, q repository.Q) UsecaseCompany {
	return &RepositoryCompany{
		C: c,
		Q: q,
	}
}

func (repo *RepositoryCompany) FindOne(params *pb.SearchCompany, key string) (*pb.Company, error) {
	var (
		company *pb.Company
		err     error
	)
	_params := &pb.Company{}

	switch key {
	case "id":
		if len(params.Id) != 0 {
			_params.Id = int64(params.Id[0])
		}
	case "name":
		if len(params.Name) != 0 {
			_params.Name = params.Name[0]
		}
	case "email":
		if len(params.Email) != 0 {
			_params.Email = params.Email[0]
		}
	case "bio":
		if len(params.Bio) != 0 {
			_params.Bio = params.Bio[0]
		}
	case "industry":
		if len(params.Industry) != 0 {
			_params.Industry = params.Industry[0]
		}
	default:
		err = errors.New("Wrong key")
	}
	if err != nil {
		log.Error(err)
		return company, err
	}

	company, err = repo.Q.FindOne(_params, key)
	if err != nil {
		log.Error(err)
		return company, err
	}

	return company, err
}

func (repo *RepositoryCompany) FindAll(params *pb.SearchCompany, limit int, offset int, key string) ([]*pb.Company, error) {
	var (
		company []*pb.Company
		err     error
	)

	company, err = repo.Q.FindAll(params, limit, offset, key)
	if err != nil {
		log.Error(err)
		return company, err
	}

	return company, err
}

func (repo *RepositoryCompany) Count(params *pb.SearchCompany) (int, error) {
	var (
		totalItems int
		err        error
	)
	return totalItems, err
}

func (repo *RepositoryCompany) AddOne(proc *pb.Process, company *pb.Company) (*pb.Process, *pb.Company, error) {
	var (
		err    error
		procTs time.Time
	)

	//build events from company
	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, company, err
	}
	prev := &pb.Company{}
	eventSource := es.EventSource{
		ID:        0,
		Name:      es.ENTITY_NAME,
		Version:   0,
		CreatedAt: procTs,
		UpdatedAt: time.Time{},
	}

	eventStreams, err := es.BuildEventsFromCompany(proc, prev, company)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.WriteNewStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	//write snapshot
	tempCompany := &pb.Company{}
	company, err = es.BuildCompanyFromEvents(eventSource, eventStreams, tempCompany)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	data, _ := json.Marshal(&company)
	ss := &es.Snapshot{
		EventSourceName: eventSource.Name,
		EventSourceID:   eventSource.ID,
		Version:         eventSource.Version,
		Data:            string(data),
		CreatedAt:       eventSource.CreatedAt,
	}
	company, err = repo.C.WriteSnapshot(ss)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	//create projection on query side
	company, err = repo.Q.AddOne(company)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	return proc, company, err
}

func (repo *RepositoryCompany) UpdateOne(proc *pb.Process, company *pb.Company) (*pb.Process, *pb.Company, error) {
	var (
		err    error
		procTs time.Time
	)

	//sleep is to ensure the data in elasticsearch is the latest
	time.Sleep(1 * time.Second)

	//get version from company
	prev, err := repo.Q.FindOne(company, "id")
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	createdAt, err := ptypes.Timestamp(prev.CreatedAt)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	//check if this version does not match with current version
	if prev.Version != company.Version {
		err = errors.New("concurrency exception : version does not match with query layer version")
		log.Error(err)
		return proc, company, err
	}

	//fill in created at from previous record and updated at using process timestamp
	eventSource := es.EventSource{
		ID:        company.Id,
		Name:      es.ENTITY_NAME,
		Version:   company.Version,
		CreatedAt: createdAt,
		UpdatedAt: procTs,
	}

	eventStreams, err := es.BuildEventsFromCompany(proc, prev, company)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.AppendToStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	//build company
	company, err = es.BuildCompanyFromEvents(eventSource, eventStreams, prev)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	if math.Mod(float64(eventSource.Version), 5) == 0 {
		data, _ := json.Marshal(&company)
		ss := &es.Snapshot{
			EventSourceName: eventSource.Name,
			EventSourceID:   eventSource.ID,
			Version:         eventSource.Version,
			Data:            string(data),
			CreatedAt:       eventSource.CreatedAt,
		}
		company, err = repo.C.WriteSnapshot(ss)
		if err != nil {
			log.Error(err)
			return proc, company, err
		}
	}

	//create projection on query side
	company, err = repo.Q.UpdateOne(company)
	if err != nil {
		log.Error(err)
		return proc, company, err
	}

	return proc, company, err
}

func (repo *RepositoryCompany) RemoveOne(proc *pb.Process, company *pb.Company) (*pb.Process, *pb.Company, error) {
	var (
		err error
	)
	return proc, company, err
}
